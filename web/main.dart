// Copyright (c) 2016, <your name>. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

library Angular2_Heroes_Dart.web;

import 'package:angular2/bootstrap.dart';
import 'package:Angular2_Heroes_Dart/app_component.dart';
import 'package:Angular2_Heroes_Dart/hero_form_component.dart';

main() {
  bootstrap(HeroFormComponent);
}
