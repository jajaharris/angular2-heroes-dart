library hero_form.hero_form_component;

import 'package:angular2/angular2.dart';
import 'package:Angular2_Heroes_Dart/models/hero.dart';

const List<String> _powers = const [
    'Really Smart',
    'Super Flexible',
    'Super Hot',
    'Weather Changer'
];

@Component(selector: 'hero-form', templateUrl: 'hero_form_component.html')
class HeroFormComponent {
    List<String> get powers => _powers;
    bool submitted = false;
    Hero model = new Hero(18, 'Dr IQ', _powers[0], 'Chuck Overstreet');
    String get diagnostic => 'DIAGNOSTIC: $model';

    onSubmit() {
        submitted = true;
    }
}
